FROM alpine:3.14 AS build
RUN apk update && apk add --no-cache ldc dmd dub alpine-sdk openssl-dev zlib-dev llvm-libunwind-static curl-static
WORKDIR /app

# Copy app source code and compile it
ENV DC=/usr/bin/dmd
COPY dub.* /app/
COPY source /app/source
RUN dub build

# tag::production[]
FROM alpine:3.14
RUN apk update && apk add --no-cache fortune llvm-libunwind
WORKDIR /app
COPY --from=build /app/fortune-d /app/
COPY views /app/views

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/app/fortune-d"]
# end::production[]
