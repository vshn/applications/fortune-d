import vibe.vibe;
import mustache;
import std.process;
import std.random;

alias MustacheEngine!(string) Mustache;
string hostname;
string appVersion = "1.2-d";

string execute(string command)
{
    auto output = "";
    auto shell = executeShell(command);
    if (shell.status == 0) output = shell.output;
    return output;
}

// tag::router[]
void fortune(HTTPServerRequest req, HTTPServerResponse res)
{
    int number = uniform(1,1000);
    string message = execute("fortune");
    auto accept = req.headers["Accept"];
    if (accept == "application/json")
    {
        Json fortune = Json.emptyObject;
        fortune["message"] = message;
        fortune["number"] = number;
        fortune["hostname"] = hostname;
        fortune["version"] = appVersion;
        res.writeJsonBody(fortune);
        return;
    }
    if (accept == "text/plain")
    {
        string str = format("Fortune %s cookie of the day #%d:\n\n%s", appVersion, number, message);
        res.writeBody(str, "text/plain");
        return;
    }

    Mustache mustache;
    auto context = new Mustache.Context;
    context["message"]  = message;
    context["number"] = number;
    context["version"] = appVersion;
    context["hostname"] = hostname;

    res.writeBody(mustache.render("views/fortune", context), "text/html");
}
// end::router[]

void main()
{
    hostname = execute("hostname");

    auto router = new URLRouter;
    router.get("/", &fortune);

    auto settings = new HTTPServerSettings;
    settings.port = 8080;
    settings.bindAddresses = ["::1", "0.0.0.0"];
    auto listener = listenHTTP(settings, router);
    scope (exit)
    {
        listener.stopListening();
    }

    logInfo("Please open http://localhost:8080/ in your browser.");
    runApplication();
}
